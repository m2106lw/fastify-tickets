// Express Modules
const express = require("express");
const app = express();
const asyncHandler = require('express-async-handler');
// For logging
const { logger } = require('./logging');

// Database
const start_database = require('./database');
// TODO: Look into handling errors better
// Start the database and if it fails then we will kill the app - idea is work in progress
start_database().then(result => result).catch(error => error) //process.exit(1))


// Body Parser Middleware
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({limit: '50mb'}));

// Cookie Parser Middleware
const cookieParser = require('cookie-parser');
// TODO: Maybe look at using a different secret key for signed cookies
const {SECRET_KEY} = require('./configuration')
app.use(cookieParser(SECRET_KEY))

// CORS Middleware
app.use(asyncHandler(async (req, res, next) => {
	const {ACCEPT_ORIGIN} = require('./configuration');
    //Enabling CORS
	//res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Origin', ACCEPT_ORIGIN);
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Methods", "DELETE,GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Authorization, Content-Type, Accept, Set-Cookie");
    next();
}));

// Not sure if this is needed
app.use(asyncHandler(async (req, res, next) => {
	// This helps with security in a minor way - avoids specific exploits for Express
	res.removeHeader("X-Powered-By");

	// Catch preflight
	if (req.method == 'OPTIONS') {
		res.status(200);
		res.send();
	}
	else {
		next();
	}
}));

// Routes Middleware
app.use('/login', require('./routes/login'));
app.use('/api', require('./routes/tokens'));

// If we get a call we can't find then send 404
app.get('*', asyncHandler(async (req, res, next) => {
	res.sendStatus(404)
}));

// This will eventuall handle errors better
app.use(asyncHandler(async (req, res, next) => {
	//console.log("The error ", error);
	next(req.error)
}))

// Error handling middleware
// TODO: Make this display helpful errors in the web
const errorHandler = (err, req, res, next) => {
	logger.warn({"category": "app", "success": false, "type": err.name, "description": err.message, "stacktrace": err.stack});
	res.status(500);
	res.send({"error": err.name, "message": err.message, "stacktrace": err.stack});
}
app.use(errorHandler);

// For some reason simply exporting app will not work, but this does
module.exports = function() {
	return app;
}
