const fs = require('fs');

// Check if the config file does exist - if it does then we will pull values from there
if (fs.existsSync('./config.js')) {
    const config = require('../config.js');

     try {
        // Setup the needed variables for the job. The environment variables will take precedence
        const NODE_ENV = process.env.NODE_ENV || config.NODE_ENV;
        const PORT = process.env.PORT || config.PORT;
        const SALT = process.env.SALT || config.SALT;
        const SECRET_KEY = process.env.SECRET_KEY || config.SECRET_KEY;
        const LOGGING_LEVEL = process.env.LOGGING_LEVEL || config.LOGGING_LEVEL;
        const DATABASE_USER =  process.env.DATABASE_USER || config.DATABASE_USER;
        const DATABASE_PASSWORD =  process.env.DATABASE_PASSWORD || config.DATABASE_PASSWORD;
        const DATABASE_SERVER =  process.env.DATABASE_SERVER || config.DATABASE_SERVER;
        const DATABASE_DATABASE =  process.env.DATABASE_DATABASE || config.DATABASE_DATABASE;
        const DATABASE_PORT = process.env.DATABASE_PORT || config.DATABASE_PORT
        // This can be a comma separated string
        // This will fail in the browser if it reaches it's default
        const ACCEPT_ORIGIN = process.env.ACCEPT_ORIGIN || config.ACCEPT_ORIGIN || "*";
        const SILENT = process.env.SILENT || config.SILENT || false;
        module.exports = {
            NODE_ENV, PORT, SALT, SECRET_KEY, LOGGING_LEVEL, DATABASE_USER, DATABASE_PASSWORD, DATABASE_SERVER, DATABASE_DATABASE, ACCEPT_ORIGIN, DATABASE_PORT,
            SILENT
        }
    }
    catch (error) {
        // Figure out how to best log this as we can't use our logging module due to circular dependacy
        console.log(`${(new Date()).toISOString()} error {"status": "failure", "category": "configuration", "error": ${error}}`);
        process.exit(1);
    }
}
else {
    // Setup the needed variables for the job. The environment variables will take precedence
    const NODE_ENV = process.env.NODE_ENV || "development";
    const PORT = process.env.PORT;
    const SALT = process.env.SALT || 15;
    const SECRET_KEY = process.env.SECRET_KEY;
    const LOGGING_LEVEL = process.env.LOGGING_LEVEL || "info";
    const DATABASE_USER =  process.env.DATABASE_USER;
    const DATABASE_PASSWORD =  process.env.DATABASE_PASSWORD;
    const DATABASE_SERVER =  process.env.DATABASE_SERVER;
    const DATABASE_DATABASE =  process.env.DATABASE_DATABASE;
    const DATABASE_PORT = process.env.DATABASE_PORT
    // This can be a comma separated string
    // This will fail in the browser if it reaches it's default
    const ACCEPT_ORIGIN = process.env.ACCEPT_ORIGIN || "*"; 
    const SILENT = process.env.SILENT || false;
    module.exports = {
        NODE_ENV, PORT, SALT, SECRET_KEY, LOGGING_LEVEL, DATABASE_USER, DATABASE_PASSWORD, DATABASE_SERVER, DATABASE_DATABASE, ACCEPT_ORIGIN, DATABASE_PORT,
        SILENT
    }
}