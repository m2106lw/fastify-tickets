module.exports = (sequelize, DataTypes) => {
	// Car Table
  	const Ticket = sequelize.define('ticket', {
        ticket_id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        data: {
          type: DataTypes.JSONB,
          allowNull: false
        }
	});

  	return Ticket;
}