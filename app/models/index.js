'use strict';
const Sequelize = require('sequelize');
// Connection info from either our config file or from env variables
const {NODE_ENV, LOGGING_LEVEL, DATABASE_USER, DATABASE_PASSWORD, DATABASE_SERVER, DATABASE_DATABASE, DATABASE_PORT} = require('../configuration')
//const {NODE_ENV, LOGGING_LEVEL} = require('../configuration')

// TODO: Add on more levels here as needed
if (LOGGING_LEVEL.toLowerCase() == "debug") {
	var logging = console.log
}
else {
	var logging = false
}

// We set up the connection info
let sequelize = new Sequelize(DATABASE_DATABASE, DATABASE_USER, DATABASE_PASSWORD, {
	host: DATABASE_SERVER,
	dialect: 'postgres',
	port: DATABASE_PORT || 3306,
	operatorsAliases: false,
	logging: false,
	pool: {
	  	max: 10,
	  	min: 0,
	  	acquire: 30000,
	  	idle: 10000
	},
	logging: logging
});

// If we are in production then we need ssl info - maybe
if (NODE_ENV.toLowerCase() === "production" || NODE_ENV.toLowerCase() === "staging") {
	const fs = require('fs');
	const {KEY_FILE, CERT_FILE} = require('../configuration');
	sequelize.dialectOptions = {
    	ssl: {
			key: fs.readFileSync(KEY_FILE),
			cert: fs.readFileSync(CERT_FILE),
      		//ca: cCA
		}
	}
}

// We grab all the files in the models directory and import to sequelize
let db = {};
db["Ticket"] = sequelize.import('./tickets')
db["User"] = sequelize.import('./users')
/* db["Account"] = sequelize.import('./accounts.js')
db["Car"] = sequelize.import('./cars.js')
db["Gas_Transaction"] = sequelize.import('./gas_transactions.js')
db["Transaction_Type"] = sequelize.import('./transaction_types.js')
db["Transaction"] = sequelize.import('./transactions.js')
db["User"] = sequelize.import('./users'); */

// We loop through and grab all associations
Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
    	db[modelName].associate(db);
  	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;