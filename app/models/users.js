module.exports = (sequelize, DataTypes) => {
	// User Table
  	const User = sequelize.define('user', {
    	user_id: {
      		type: DataTypes.INTEGER,
      		autoIncrement: true,
      		primaryKey: true
    	},
    	username: {
      		type: DataTypes.STRING,
			allowNull: false,
			unique: true
    	},
    	password: {
      		type: DataTypes.TEXT,
      		allowNull: false
    	}
  	})

  	return User;
}