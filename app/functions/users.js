const sequelize = require('../models/index').sequelize;
const User = require('../models/index').User;

// This function will create a user and return the new User
const create_user = async (username, password) => {
    return await sequelize.transaction((transaction) => {
        return User.create({
            username: username,
            password: password
        },
        {
            transaction: transaction,
            raw: true
        })
    }).then(result => result.dataValues)
    .catch(error => {
        throw new Error(error);
    })
}

// This function will update the user's username
const update_username = async (user_id, username) => {
    return await sequelize.transaction((transaction) => {
        return User.update({
                username: username
            }, 
            {
                where: {user_id: user_id},
                transaction: transaction
            })
    }).then(result => result)
    .catch(error => {
        throw new Error(error)
    })
}

// Update the user's password
const update_password = async (user_id, password) => {
    return await sequelize.transaction((transaction) => {
        return User.update({
                password: password
            },
            {
                where: {user_id: user_id},
                transaction: transaction
            })
    }).then(result => result)
    .catch(error => {
        throw new Error(error)
    })
}

// This function will find a user based on their username and return the user_id and password
const find_user = async (username) => {
    return await User.findOne({
        attributes: ['user_id', 'password'],
        where: {username: username},
        raw: true
    })
    .then(result => result)
    .catch(error => {
        throw new Error(error)
    })
}

// This function will delete the user and hopefully everything related to them
const delete_user = async (user_id) => {
    return await sequelize.transaction((transaction) => {
        return User.destroy({
            where: {user_id: user_id}
        },
        {
            transaction: transaction,
            raw: true
        })
    }).then(result => result.dataValues)
    .catch(error => {
        throw new Error(error);
    })
}

module.exports = {
    create_user, update_username, find_user, update_password, delete_user
}