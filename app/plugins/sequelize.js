const fp = require('fastify-plugin')

// modified from https://github.com/lyquocnam/fastify-sequelize
function plugin (fastify, options) {
	const name = options.name
	const instance = options.instance

	//return decorate

	// TODO: Look into authenticating later on
 	return instance.sequelize.authenticate()
		.then(() => {
			return instance.sequelize.sync()
				.then(decorate)
				.catch(error => {
					throw new Error(error)
				})
		})
		.catch(error => {
			throw new Error(error)
		})

   	function decorate () {
    	fastify.decorate(name, instance)
    	fastify.addHook('onClose', (fastifyInstance, done) => {
      	instance.sequelize.close()
        	.then(done)
        	.catch(done)
    	})
  	}
}

module.exports = fp(plugin)