'use strict'

const jwt = require('jsonwebtoken');
const {SECRET_KEY} = require('../configuration');
// Signed Cookies
const signature = require('cookie-signature');

module.exports = async (fastify, opts, next) => {
	// This will just make sure we even recieved the credentials at all
	// If not then return the appropriate settings
	fastify.addHook('preHandler', async (request, reply, done) => {
		// First need to grab the cookie itself - might be an issue if more than one arrives
		// Next we need to unsign the token - TODO: need to check if true or not
		// TODO: Catch missing cookie
		let token_cookie = request.cookies.token
		let token = signature.unsign(token_cookie, SECRET_KEY)

		if (token) {
			// If the token is good then we will continue on to whatever call was made
			// Otherwise just send back the error
			jwt.verify(token, SECRET_KEY, (err, decoded) => {
				if (err) {
					// TODO: Send the specific jwt errors
					reply.code(401);
					reply.send({"success": false, "error": {err}});
				}
				else {
					request.decoded = decoded;
					done()
				}
			});
		}
		else {
			reply.code(401)
			reply.send({});
		}
	})

	fastify.register(require('./tokens/tickets'))
}