const users = require('express').Router();
const asyncHandler = require('express-async-handler');
const {logAPICall} = require('../../logging');
const {delete_user} = require('../../functions/users');
	
users.delete("/users", asyncHandler(async (req, res, next) => {
	logAPICall(req.baseUrl + req.route.path, req.decoded.user_id);

	const user_id = req.decoded.user_id;

	await delete_user(user_id)
	.then(data => {
		res.status(200);
		// TODO: Remove cookie if in response?
		res.json(data);
	})
	.catch(error => {
		throw new Error(error)
	})
}));	

module.exports = users;