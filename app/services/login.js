'use strict'

// Password encoding
const bcrypt = require('bcrypt');
// Add basic-auth
const auth = require('basic-auth');
// JSON Web Token
const jwt = require('jsonwebtoken');
// For logging
const { logger } = require('../logging');
// Config variables
const {SALT, SECRET_KEY} = require('../configuration')
// Signed Cookies
const signature = require('cookie-signature');
// SQL Functions
const {create_user, find_user} = require('../functions/users')

module.exports = async (fastify, opts, next) => {
	// This will just make sure we even recieved the credentials at all
	// If not then return the appropriate settings
	fastify.addHook('preHandler', async (request, reply) => {
		let credentials = auth(request);
		if (!credentials) {
			reply.code(401);
			reply.header('WWW-Authenticate', 'Basic');
			reply.send({'error': 'Access denied'});
		}
		else {
			return
		}
	})

	fastify.decorateReply('token', function (user_id) {
		// Now create the token and store their user_id inside for later calls
 		jwt.sign({ "user_id": user_id }, SECRET_KEY, { algorithm: 'HS512', expiresIn: '8h' }, (err, token) => {
			if (err) {
				this.code(401);
				this.send({"success": false, "error": err});
			}
			else {
				// Get 8 hrs from now, same as the token. There may be a way to grab the same expiration
				let expiration = new Date(Math.floor(Date.now()) + (2 * 60 * 60 * 1000))
				let signed_token = signature.sign(token, SECRET_KEY)
				// TODO: Check the NODE_ENV to set secure status
				this.setCookie('token', signed_token, {path: '/', httpOnly: true, expires: expiration, secure: false})
				this.code(200)
				this.send({"success": true, "message": "Login successful"})
			}
		});
	})	  

	// This if for is the user is new and needs to have their account setup. A jwt will be returned at the end, just like above.
	fastify.post("/users", async (request, reply) => {
		let credentials = auth(request);
		let username = credentials["name"];
		let password = credentials["pass"];

		let hash = await bcrypt.hash(password, SALT)
			.then(hash => hash)
			.catch(error => {
				throw new Error(error);
			});

		// This should return the user_id and then we will proceed to the creation of the jwt
		await create_user(username, hash)
			.then(new_user => {
				reply.token(new_user.user_id)
			})
			.catch(error => {
				console.log(error)
				// Need to check on error type so we can route where need
				reply.code(401);
				reply.send(`${username} already exists`)
			})
	});

	fastify.post("/", async (request, reply) => {
		let credentials = auth(request);
		let username = credentials["name"];
		let password = credentials["pass"];
	
		// TODO: We need to handle errors better
		// We are going to grab the user's ID based on their username
		let user = await find_user(username)
			.then(user => user)
			.catch(error => {
				throw new Error(error)
			})

		if (user) {
			let user_id = user.user_id;
			let password_hash = user.password;

			let compare = await bcrypt.compare(password, password_hash)
				.then(result => result)
				.catch(error => {
					throw new Error(error);
				})

			// TODO: Improve the error messages here
			if (compare) {
				reply.token(user_id)
			}
			else {
				reply.code(401);
				reply.send(`Bad password for ${username}`)
			}
		}
		else {
			reply.code(401);
			reply.header('WWW-Authenticate', 'Basic');
			reply.send('User not found');		
		}
	});
}
