#!/bin/env node
'use strict'

// For logging with winston - I feel it gives me a little more control
// At least until I can figure out Pino
const { logger } = require('./app/logging');

// Config variables
const {PORT} = require('./app/configuration');

// Require the framework and instantiate it
const fastify = require('fastify')({
	logger: false
})

// Database
// register sequelize service

// Services
fastify.register(require('fastify-cookie', {}))
fastify.register(require('./app/services/login'), {'prefix': '/login'})
fastify.register(require('./app/services/tokens'), {'prefix': '/api'})
fastify.after(err => {
    if (err) console.log(err)
})

// Plugins
const ticketing = require('./app/models/index');
const fsequelize = require('./app/plugins/sequelize')
fastify.register(fsequelize, {instance: ticketing, name: 'ticketing'}).ready((error) => {
	// TODO: This doesn't work
/* 	fastify.ticketing.sequelize.authenticate()
		.then(() => {
			fastify.ticketing.sequelize.sync()
				.catch(error => {
					throw new Error(error)
				})
		})
		.catch(error => {
			throw new Error(error)
		}) */
	if (error) {
		console.log('Unable to connect to the database:', err)
	}
})

// Start the server
fastify.listen(PORT, (err, address) => {
	if (err) {
		fastify.log.error(err)
		process.exit(1)
	}
	//logger.info(`server listening on ${address}`)
	fastify.log.info(`server listening on ${address}`)
})

// Body Parser Middleware
/* const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({limit: '50mb'})); */

// Cookie Parser Middleware
/* const cookieParser = require('cookie-parser');
// TODO: Maybe look at using a different secret key for signed cookies
const {SECRET_KEY} = require('./configuration')
app.use(cookieParser(SECRET_KEY)) */

// CORS Middleware
/* app.use(asyncHandler(async (req, res, next) => {
	const {ACCEPT_ORIGIN} = require('./configuration');
    //Enabling CORS
	//res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Origin', ACCEPT_ORIGIN);
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Methods", "DELETE,GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Authorization, Content-Type, Accept, Set-Cookie");
    next();
}));

// Not sure if this is needed
app.use(asyncHandler(async (req, res, next) => {
	// This helps with security in a minor way - avoids specific exploits for Express
	res.removeHeader("X-Powered-By");

	// Catch preflight
	if (req.method == 'OPTIONS') {
		res.status(200);
		res.send();
	}
	else {
		next();
	}
}));

// Routes Middleware
app.use('/login', require('./routes/login'));
app.use('/api', require('./routes/tokens'));

// If we get a call we can't find then send 404
app.get('*', asyncHandler(async (req, res, next) => {
	res.sendStatus(404)
}));

// This will eventuall handle errors better
app.use(asyncHandler(async (req, res, next) => {
	//console.log("The error ", error);
	next(req.error)
}))

// Error handling middleware
// TODO: Make this display helpful errors in the web
const errorHandler = (err, req, res, next) => {
	logger.warn({"category": "app", "success": false, "type": err.name, "description": err.message, "stacktrace": err.stack});
	res.status(500);
	res.send({"error": err.name, "message": err.message, "stacktrace": err.stack});
}
app.use(errorHandler);

// For some reason simply exporting app will not work, but this does
module.exports = function() {
	return app;
} */
